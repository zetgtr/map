<?php

use Catalog\Enums\CatalogEnums;
use Laravel\Fortify\Features;
use Map\Enums\MapEnums;

return [
    'title' => 'Яндекс карта',
    'menu_links' => [
        MapEnums::CONSTRUCTOR->value => ['route' => 'admin.map', 'name' => 'Конструктор'],
        MapEnums::CATEGORY->value => ['route' => 'admin.map.category.index', 'name' => 'Категории'],
        MapEnums::TYPE->value => ['route' => 'admin.map.type.index', 'name' => 'Типы'],
        MapEnums::SETTINGS->value => ['route' => 'admin.map.settings.index', 'name' => 'Настройки']
    ],
    'menu_settings' => [
        MapEnums::KEY->value => ['url' => MapEnums::KEY->value, 'name' => 'Ключи'],
        MapEnums::BUTTONS->value => ['url' => MapEnums::BUTTONS->value, 'name' => 'Кнопки'],
        MapEnums::SEO->value => ['url' => MapEnums::SEO->value, 'name' => 'SEO'],
    ],
    'menu_type' => [
        MapEnums::TYPE->value => ['url' => MapEnums::TYPE->value, 'name' => 'Типы'],
        MapEnums::ICONS->value => ['url' => MapEnums::ICONS->value, 'name' => 'Иконки'],
    ],
    'fillable' => [
        'marker' => [
            'category_id','title','cord','address','description','type_id'
        ],
        'icon' => [
            'url'
        ],
        'type' => [
            'title','icon'
        ],
        'zone' => [
            'title','cords','description','color','width','fill'
        ],
        'category' => [
            'title'
        ],
        'settings' => [
            'api_key','lang','seo_title','seo_url','seo_description',
            'title','seo_keywords','btn_category','btn_type','btn_point',
            'btn_category_check','btn_type_check','btn_point_check','btn_search_check','btn_search'
        ]
    ],
    'request' => [
        'zone' => [
            'title'=> ['required','string'],
            'cords'=> ['required','json'],
            'color'=> ['required','string'],
            'width'=> ['required','integer'],
            'fill'=> ['required','string'],
            'description'=> ['nullable','string']
        ],
        'category' => [
            'title' => ['required','string'],
        ],
        'icon' => [
            'url' => ['required','string'],
            'icon' => ['required', 'file', 'dimensions:min_width=27,min_height=32,max_width=27,max_height=32'],
        ],
        'type' => [
            'title' => ['required','string'],
            'icon' => ['required'],
        ],
        'marker' => [
            'cord' => ['required','json'],
            'title' => ['nullable','string'],
            'description' => ['nullable','string'],
            'type_id' => ['nullable'],
            'category_id' => ['nullable'],
            'address' => ['required','string'],
        ],
        'settings' => [
            'api_key' => ['required','string'],
            'lang' => ['required'],
            'seo_title' => ['nullable'],
            'title' => ['nullable'],
            'seo_url' => ['nullable'],
            'seo_keywords' => ['nullable'],
            'btn_category' => ['required'],
            'btn_type' => ['required'],
            'btn_point' => ['required'],
            'btn_category_check' => ['required'],
            'btn_type_check' => ['required'],
            'btn_point_check' => ['required'],
            'btn_search_check' => ['required'],
            'btn_search' => ['required'],
            'seo_description' => ['nullable']
        ]
    ],
];
