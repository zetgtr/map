<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('map_settings', function (Blueprint $table) {
            $table->string('btn_search')->default('Поиск');
            $table->boolean('btn_search_check')->default(true);
        });
    }

    public function down(): void
    {
        Schema::table('map_settings', function (Blueprint $table) {
            $table->dropColumn('btn_search');
            $table->dropColumn('btn_search_check');
        });
    }
};
