<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::table('map_settings', function (Blueprint $table) {
            $table->string('btn_category')->default('Категории');
            $table->boolean('btn_category_check')->default(true);
            $table->string('btn_type')->default('Типы');
            $table->boolean('btn_type_check')->default(true);
            $table->string('btn_point')->default('Точки на карте');
            $table->boolean('btn_point_check')->default(true);

        });
    }

    public function down(): void
    {
        Schema::table('map_settings', function (Blueprint $table) {
            $table->dropColumn('btn_category');
            $table->dropColumn('btn_type');
            $table->dropColumn('btn_point');
            $table->dropColumn('btn_point_check');
            $table->dropColumn('btn_type_check');
            $table->dropColumn('btn_point_check');
        });
    }
};
