<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('map_markers', function (Blueprint $table) {
            $table->id();
            $table->foreignId('category_id')->nullable()->constrained('map_categories');
            $table->foreignId('type_id')->nullable()->constrained('map_types');
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('address');
            $table->json('cord');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('map_markers');
    }
};
