<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    public function up(): void
    {
        Schema::create('map_zones', function (Blueprint $table) {
            $table->id();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('color')->nullable();
            $table->string('fill')->nullable();
            $table->integer('width')->nullable();
            $table->json('cords');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('map_zones');
    }
};
