<?php

namespace Map\Seeders;

use Illuminate\Database\Seeder;

class IconSeeder extends Seeder
{
    public function run(): void
    {
        \DB::table('map_icons')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['url'=>'/storage/map/icons/65ef15a0d5f0f.webp'],
            ['url'=>'/storage/map/icons/65ef15a5cdac0.webp'],
            ['url'=>'/storage/map/icons/65ef15ac4ee70.webp'],
            ['url'=>'/storage/map/icons/65ef15b636c16.webp'],
            ['url'=>'/storage/map/icons/65ef15b1125fc.webp'],
            ['url'=>'/storage/map/icons/65ef15bb6e8f7.webp'],
            ['url'=>'/storage/map/icons/65ef157f37617.webp'],
            ['url'=>'/storage/map/icons/65ef158a0d37a.webp'],
            ['url'=>'/storage/map/icons/65ef159ac90eb.webp'],
            ['url'=>'/storage/map/icons/65ef15957ff1e.webp']
        ];
    }
}
