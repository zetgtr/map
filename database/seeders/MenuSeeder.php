<?php

namespace Map\Seeders;

use Illuminate\Database\Seeder;

class MenuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        \DB::table('menus')->insert($this->getData());
    }

    public function getData()
    {
        return [
            ['id'=>979,'name'=>'Яндекс карта','position'=>'left','logo'=>'fal fa-map-marked-alt','controller'=>'Map\Http\Controllers\MapController','url'=>'map','parent'=>5,"controller_type"=>"invocable", 'order'=>4],
        ];
    }
}
