<template id="category_template">
    <div class="info__container">
        <div class="info__header">
            <h3 class="card-title">Категории</h3>
            <button type="button" class="btn-close"></button>
        </div>
        <hr>
        <div class="info__body form-control">
            @foreach ($categories as $category)
                <div class="info__item category_item" data-id="{{ $category->id }}">
                    <p>{{ $category->title }}</p>
                    <button type="button" class="btn-close"></button>
                </div>
            @endforeach
        </div>
    </div>

</template>
