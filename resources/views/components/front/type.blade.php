<template id="type_template">
    <div class="info__container">
        <div class="info__header">
            <h3 class="card-title">Типы</h3>
            <button type="button" class="btn-close"></button>
        </div>
        <hr>
        <div class="info__body form-control">
            @foreach ($types as $type)
                <div class="info__item category_item" data-id="{{ $type->id }}">
                    <p>{{ $type->title }}</p>
                    <button type="button" class="btn-close"></button>
                </div>
            @endforeach
        </div>
    </div>

</template>
