<div class="tab-pane fide" id="{{ \Map\Enums\MapEnums::BUTTONS->value }}" role="tabpanel">
    <div class="row">
        <div class="col-lg-3">
            <div class="form-group">
                    <label class="custom-switch mt-5 ps-0">
                        <input type="checkbox" name="btn_category_check" class="custom-switch-input" @checked(old('btn_category_check', $settings->btn_category_check))>
                        <span class="custom-switch-indicator"></span>
                        <span class="custom-switch-description ">Категории</span>
                    </label>
                <input type="text" class="form-control @error('btn_category') is_invalid @enderror"
                       name="btn_category" value="{{ old('btn_category',$settings ? $settings->btn_category : "Категории") }}">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="custom-switch mt-5 ps-0">
                    <input type="checkbox" name="btn_type_check" class="custom-switch-input" @checked(old('btn_type_check', $settings->btn_type_check))>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description ">Типы</span>
                </label>
                <input type="text" class="form-control @error('btn_type') is_invalid @enderror"
                       name="btn_type" value="{{ old('btn_type',$settings ? $settings->btn_type : "Типы") }}">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="custom-switch mt-5 ps-0">
                    <input type="checkbox" name="btn_point_check" class="custom-switch-input" @checked(old('btn_point_check', $settings->btn_point_check))>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description ">Точки на карте</span>
                </label>
                <input type="text" class="form-control @error('btn_point') is_invalid @enderror"
                       name="btn_point" value="{{ old('btn_point',$settings ? $settings->btn_point : "Точки на карте") }}">
            </div>
        </div>
        <div class="col-lg-3">
            <div class="form-group">
                <label class="custom-switch mt-5 ps-0">
                    <input type="checkbox" name="btn_search_check" class="custom-switch-input" @checked(old('btn_search_check', $settings->btn_search_check))>
                    <span class="custom-switch-indicator"></span>
                    <span class="custom-switch-description ">Поиск</span>
                </label>
                <input type="text" class="form-control @error('btn_search') is_invalid @enderror"
                       name="btn_search" value="{{ old('btn_search',$settings ? $settings->btn_search : "Поиск") }}">
            </div>
        </div>
    </div>
</div>
