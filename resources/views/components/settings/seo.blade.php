<div class="tab-pane fide" id="{{ \Map\Enums\MapEnums::SEO->value }}" role="tabpanel">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label>Заголовок страницы</label>
                <input type="text" class="form-control @error('title') is_invalid @enderror"
                       name="title" value="{{ old('title',$settings ? $settings->title : "") }}">
            </div>
        </div>

        <div class="col-lg-6">
            <div class="form-group">
                <label>Seo title</label>
                <input type="text" class="form-control @error('seo_title') is_invalid @enderror"
                       name="seo_title" value="{{ old('seo_title',$settings ? $settings->seo_title : "") }}">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Адрес страницы</label>
                <input type="text" class="form-control @error('seo_url') is_invalid @enderror"
                       name="seo_url" value="{{ old('seo_url',$settings ? $settings->seo_url : "") }}">
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label>Seo keywords</label>
                <input type="text" class="form-control @error('seo_keywords') is_invalid @enderror"
                       name="seo_keywords" value="{{ old('seo_keywords',$settings ? $settings->seo_keywords : "") }}">
            </div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <label>Seo description</label>
                <textarea type="text" class="form-control @error('seo_description') is_invalid @enderror"
                          name="seo_description">{{ old('seo_description',$settings ? $settings->seo_description : "") }}</textarea>
            </div>
        </div>
    </div>
</div>
