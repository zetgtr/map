
<template id="type_template">
    <div>
        <h3 class="card-title"><span>Типы</span><button type="button" class="btn-close"><i class="fas fa-times"></i></button></h3>
        <hr>
        <div class="form-control" style="padding: 0;overflow: auto;max-height: 535px">
            @foreach($types as $type)
                <div class="category_item" data-id="{{ $type->id }}">
                    {{$type->title}}
                </div>
            @endforeach
        </div>
    </div>

</template>
