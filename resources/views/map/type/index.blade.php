@extends('layouts.admin')
@section('title',"Типы")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <x-admin.navigatin-js :links="$typeLinks" />
        <div class="card-body">
            <x-warning />
            <div class="tab-content">

                <x-map::type.type />
                <x-map::type.icons />
{{--            <form action="{{ route('admin.map.settings.store') }}" method="POST">--}}
{{--                @csrf--}}
{{--                <x-map::map.settings />--}}
{{--                <input type="submit" value="Сохранить" class="btn btn-sm btn-success">--}}
{{--            </form>--}}
            </div>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.map")}}">{{ config('map.title') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Типы</li>
        </ol>
    </div>
@endsection
