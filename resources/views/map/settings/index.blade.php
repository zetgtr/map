@extends('layouts.admin')
@section('title',"Настройки")
@section('content')
    <div class="card">
        <x-admin.navigation :links="$links" />
        <x-admin.navigatin-js :links="$settingsLinks" />
        <div class="card-body">
            <x-warning />
            <form action="{{ route('admin.map.settings.store') }}" method="POST">
                @csrf
                <x-map::map.settings />
                <input type="submit" value="Сохранить" class="btn btn-sm btn-success">
            </form>
        </div>
    </div>
@endsection
@section('breadcrumb')
    <div>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{route("admin.index")}}">Главная</a></li>
            <li class="breadcrumb-item"><a href="{{route("admin.map")}}">{{ config('map.title') }}</a></li>
            <li class="breadcrumb-item active" aria-current="page">Настройки</li>
        </ol>
    </div>
@endsection
