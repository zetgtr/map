@extends('layouts.inner')
@section('title', $settings->seo_title)
@section('keywords', $settings->seo_keywords)
@section('description', $settings->seo_description)
@section('content')
    @vite('resources/assets/scss/map/map-front.scss')
    <script src="https://api-maps.yandex.ru/v3/?apikey={{ $settings->api_key }}&lang={{ $settings->lang }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/robust-point-in-polygon@1.0.3/robust-pnp.min.js"></script>
    <div class="content">
        <div class="innerPage-header">
            {{-- <x-front.breadcrumbs :breadcrumbs="$bredcrambs" /> --}}
            <div class="container">
                <h1>{!! $settings->title !!}</h1>
            </div>
        </div>
        <section class="map">
            <div class="container">
                <div class="map__wrapper">
                    <div class="map__info container_info"></div>
                    <div class="map__window container_map">
                        <div id="map" style="width: 100%; height: 600px"></div>
                    </div>
                </div>
                <x-map::constructor.mapdata :categories="$categories" />
            </div>
        </section>
    </div>
    <x-map::front.category :categories="$categories" />
    <x-map::front.type :types="$types" />
    <x-map::front.marker />
    <x-map::front.hint />

    <input type="hidden" id="markers" value="{{ $markers }}">
    <input type="hidden" id="settings" value="{{ $settings }}">
    <input type="hidden" id="zone_data" value="{{ $zones }}">
    @vite('resources/js/map/front/index.js')
@endsection
