class Btn {
    constructor(Front) {
        this.front = Front
        this.setBtn()
    }

    setBtn(){
        if (this.front.settings.btn_category_check)
            this.addBtn(this.front.settings.btn_category,'top left',this.front.category.open.bind(this.front.category))
        if (this.front.settings.btn_type_check)
            this.addBtn(this.front.settings.btn_type,'top left',this.front.type.open.bind(this.front.type)).then(()=>this.getBtn(this.front.settings.btn_type,'type_map_btn'))
        if (this.front.settings.btn_point_check)
            this.addBtn(this.front.settings.btn_point,'bottom left',this.front.markerAll.open.bind(this.front.markerAll))
        if (this.front.settings.btn_search_check)
            this.addBtn(this.front.settings.btn_search,'top right',this.front.markerAll.open.bind(this.front.markerAll,this.front.settings.btn_search))
    }

    async addBtn(text,position,fun){
        const {YMapControls, YMapControlButton} = ymaps3;

        const controls = new YMapControls({position,orientation: 'horizontal'});

        const button = new YMapControlButton({
            text,
            onClick: fun
        });
        // button.className = className
        controls.addChild(button);
        this.front.map.addChild(controls);
    }

    getBtn(text,className){
        const btn = this.findElementByText(text)
        btn.closest('.ymaps3x0--controls').classList.add(className)
    }
    findElementByText(text) {
        const buttons = document.querySelectorAll('.ymaps3x0--button.ymaps3x0--control-button');

        for (const button of buttons) {
            if (button.textContent.includes(text)) {
                return button;
            }
        }
        return null;
    }
}

export default Btn
