class MarkerAll {
    constructor(Front) {
        this.front = Front;
        this.containerInfo = document.querySelector(".container_info");
        this.containerMap = document.querySelector(".container_map");
    }

    open(search = false) {
        this.template = document
            .querySelector("#marker_template")
            .content.children[0].cloneNode(true);
        this.template
            .querySelector(".btn-close")
            .addEventListener("click", this.close.bind(this));
        const withMap = this.containerMap.offsetWidth;
        if (!window.openCheck) {
            this.containerMap.style.width = withMap - 300 + "px";
            this.containerInfo.style.width = "300px";
            this.containerInfo.style.marginRight = "20px";
        }
        this.containerInfo.innerHTML = "";







        if(search){
            const container = this.template.querySelector(".container_marker_all");
            this.template.querySelector('.card-title').innerText = search
            // container_marker_all
            const searchNode = document.createElement('input')
            searchNode.classList.add('form-control')
            searchNode.addEventListener('input',()=>{
                this.setMarkerData(this.front.marker.markers.filter(marker=>{
                    return (marker.item.title.toLowerCase().includes(searchNode.value.toLowerCase()) || marker.item.address.toLowerCase().includes(searchNode.value.toLowerCase())) && searchNode.value !== ''
                }))
            })
            container.before(searchNode)
        }else {
            this.setMarkerData(this.front.marker.markers)
        }



        this.containerInfo.append(this.template);
        window.openCheck = true;

    }


    setMarkerData(markers){
        const container = this.template.querySelector(".container_marker_all");
        container.innerHTML = "";
        markers.forEach((el) => {
            let dataMarkers = JSON.parse(this.front.marker.markersNode.value);
            const item = dataMarkers.filter((item) => item.id === el.id)[0];
            const itemNode = document.createElement("div");
            itemNode.classList.add("category_item", "info__item");
            itemNode.dataset.id = item.id;
            itemNode.innerHTML = item.title + "<br>" + "Адрес: " + item.address;
            itemNode.dataset.cord = item.cord;
            container.append(itemNode);
        });
        this.addEvent();
    }

    addEvent() {
        this.template
            .querySelectorAll(".category_item")
            .forEach((el) =>
                el.addEventListener("click", this.event.bind(this, el))
            );
    }

    event(el) {
        const cord = JSON.parse(el.dataset.cord);
        console.log(this.front.marker);
        this.front.marker.setCordMap([cord[1], cord[0]]);
    }

    close() {
        this.containerInfo.innerHTML = "";
        this.containerMap.style.width = "100%";
        this.containerInfo.style.width = "0px";
        this.containerInfo.style.marginRight = "0px";
        window.openCheck = false;
    }
}

export default MarkerAll;
