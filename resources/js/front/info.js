class Info {
    constructor(Front) {
        this.front = Front
    }

    openInfo(el){
        const container = el.closest('.ymaps3x0--marker')
        document.querySelectorAll('.marker__info').forEach(el=>{
            let container_el = el.closest('.ymaps3x0--marker')
            el.classList.add('d-none')
            container_el.style.zIndex = 0
        })
        container.style.zIndex = 4
        el.querySelector('.marker__info').classList.remove('d-none')
        el.querySelector('.marker__close-button').addEventListener('click',()=>{
            el.querySelector('.marker__info').classList.add('d-none')
            container.style.zIndex = 0
        })
    }

    setInfo(){
        document.querySelectorAll('.map_data_category_item').forEach(el=>el.classList.add('d-none'))
        document.querySelectorAll('.map_data_item').forEach(el=>el.classList.add('d-none'))
        this.front.marker.markers.forEach(el=>{
            if(el.item.category_id){
                document.querySelector('.marker_data_item_'+el.id).classList.remove('d-none')
                document.querySelector('.map_data_category_'+el.item.category_id).classList.remove('d-none')
            }
        })
    }
}

export default Info
