import {Map} from "../admin/Map";
import Marker from "./marker";
import Events from "./events";
import Info from "./info";
import Btn from "./btn";
import Category from "./category";
import Type from "./type";
import MarkerAll from "./marker_all";
import Zone from "./zone";

class Front extends Map{
    async init(){
        await ymaps3.ready;
        window.openCheck = false
        this.settings = JSON.parse(document.getElementById('settings').value)
        this.marker = new Marker(this)
        this.events = new Events(this)
        this.info = new Info(this)
        this.category = new Category(this)
        this.type = new Type(this)
        this.markerAll = new MarkerAll(this)
        this.btn = new Btn(this)
        this.zone = new Zone(this)
    }
}
$(document).ready(()=>{
    const map = new Front()
    map.init()
})
