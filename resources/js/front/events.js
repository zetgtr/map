class Events {
    constructor(Front) {
        this.front = Front;
        this.initMapEvents();
    }

    initMapEvents() {
        const { YMapListener } = ymaps3;
        const pageHeader = document.querySelector(".innerPage-header");

        document.querySelectorAll(".map_data_item").forEach((el) =>
            el.addEventListener("click", () => {
                const cord = JSON.parse(el.dataset.cord);
                this.front.marker.setCordMap([cord[1], cord[0]]);

                if (pageHeader) {
                    pageHeader.scrollIntoView({
                        behavior: "smooth",
                        block: "start",
                    });
                }
            })
        );
        this.front.map.addChild(
            new YMapListener({
                onActionStart: this.createBehaviorEventHandler.bind(this, true),
                onActionEnd: this.createBehaviorEventHandler(false),
            })
        );
    }

    createBehaviorEventHandler = (isStart) => {
        const context = this;

        return function (object) {};
    };
}

export default Events;
