import Modal from '../modal'
class Zone {
    constructor(admin) {
        this.admin = admin
        this.containerInfo = document.querySelector('.container_info')
        this.containerMap = document.querySelector('.container_map')
        this.dataNode = document.querySelector('#zone_data')
        this.data = JSON.parse(this.dataNode.value)
        this.cord = [[]]
        this.color = '#006efc'
        this.fill = '#3838DB'
        this.width = 4
        this.id = 0
        this.polygon = null
        this.polygons = []
        this.markers = []
        this.setData()
    }

    open(id){
        this.template = document.querySelector('#zone_template').content.children[0].cloneNode(true)
        this.template.querySelector('.btn-close').addEventListener('click',this.close.bind(this))
        new Modal(this.template)
        if (id > 0){
            const polygon = this.polygons.filter(el=>el.id === id)[0]
            this.template.querySelector('.btn-danger').classList.remove('d-none')
            this.template.querySelector('.btn-danger').addEventListener('click',()=>{
                if(confirm('Вы точно хотите удалить зону?')){
                axios.delete('/admin/map/zone/'+id).then(({data})=>{
                    if(data.status){
                        window.location.reload()
                    }
                })}
            })
            this.set(polygon,polygon.cord)
            this.template.querySelector('.card-title span').innerText = "Редактировать зону"
            this.template.querySelector('input[name=title]').value = this.title
            this.template.querySelector('textarea[name=description]').value = this.description
            this.template.querySelector('input[name=width_border]').value = this.width
            this.template.querySelector('input[name=color_border]').value = this.color
            this.template.querySelector('input[name=color_fill]').value = this.fill
        }else {
            this.template.querySelector('.btn-danger').classList.add('d-none')
            this.template.querySelector('.card-title span').innerText = "Добавить зону"
            this.cord = [[]]
            this.color = '#006efc'
            this.fill = '#3838DB'
            this.title = ''
            this.description = ''
            this.width = 4
            this.id = 0
            this.polygon = null
            this.markers = []
        }
        document.querySelectorAll('.ymaps3x0--button').forEach(el=>{
            if(!el.closest('.ymaps3x0--controls').classList.contains('zone_map_btn'))
                el.addEventListener('click',this.setDataPolygons.bind(this))
        })
        const withMap = this.containerMap.offsetWidth
        if(!window.openCheck){
            this.containerMap.style.width = (withMap - 300) + "px"
            this.containerInfo.style.width = "300px"
            this.containerInfo.style.marginRight = "20px"
        }
        this.containerInfo.innerHTML = ''
        this.containerInfo.append(this.template)
        window.openCheck = true

        this.addEvent(id)
    }

    setDataPolygons(){
        this.polygons.forEach(el=>{
            this.admin.map.removeChild(el.polygon)
            if(el.id !== 0) {
                this.set(el, el.cord)
            }else {
                el.markers.forEach(el=>{
                    this.admin.map.removeChild(el)
                })
            }
        })
        this.cord = [[]]
        this.description = ''
        this.title = ''
        this.color = '#006efc'
        this.fill = '#3838DB'
        this.width = 4
        this.id = 0
        this.polygon = null
        this.markers = []
        this.admin.map.removeChild(this.listener)
    }

    set(el,cords,drag=false){
        this.fill = el.fill
        this.markers = el.markers ?? []
        this.title = el.title
        this.description = el.description
        this.polygon = el.polygon
        this.color = el.color
        this.width = el.width
        this.cord = cords
        this.id = el.id
        if(!drag)
            this.setMarker()
        this.setPolygon()
    }

    setData(){
        if(this.polygons){
            this.polygons.forEach(el=>{
                el.markers.forEach(marker=>{
                    this.admin.map.removeChild(marker)
                })
                this.admin.map.removeChild(el.polygon)
            })
        }
        JSON.parse(this.dataNode.value).forEach(el=>{
            this.set(el,JSON.parse(el.cords))
        })
    }

    close(){
        this.containerInfo.innerHTML = ''
        this.containerMap.style.width = "100%"
        this.containerInfo.style.width = "0px"
        this.containerInfo.style.marginRight = "0px"
        window.openCheck = false
        this.admin.map.removeChild(this.listener)
        this.setData()
        this.setDataPolygons()
    }

    addEvent(id){
        if(id > 0)
            id = id
        else
            id = null
        const {YMapListener} = ymaps3;
        if(this.listener)
        this.admin.map.removeChild(this.listener)
        this.listener = new YMapListener({
            layer: 'any',
            onFastClick : this.onMapClick.bind(this),
        })
        this.template.querySelector('input[name=color_border]').addEventListener('input',this.colorBorder.bind(this))
        this.template.querySelector('input[name=color_fill]').addEventListener('input',this.colorFill.bind(this))
        this.template.querySelector('input[name=width_border]').addEventListener('input',this.widthBorder.bind(this))

        this.template.querySelector('input[type=submit]').addEventListener('click',this.save.bind(this,id))
        this.admin.map.addChild(this.listener)
    }

    widthBorder(){
        this.width = this.template.querySelector('input[name=width_border]').value
        this.setPolygon()
    }
    colorBorder(){
        this.color = this.template.querySelector('input[name=color_border]').value
        this.setPolygon()
    }

    colorFill(){
        this.fill = this.template.querySelector('input[name=color_fill]').value
        this.setPolygon()
    }

    onMapClick(object,event) {
        this.cord[0].push(event.coordinates)
        this.setMarker()
        this.setPolygon()
    }

    setMarker(){
        const { YMapMarker } = ymaps3;
        this.markers.forEach(el=>{
            this.admin.map.removeChild(el)
        })
        this.markers = [];
        this.cord[0].forEach((el,key)=>{
            const data = {
                class: this,
                key,
                id: this.id
            }
            const template = document.querySelector('#marker_zone').content.children[0].cloneNode(true)
            template.title = key + 1
            if(key === 0)
                template.querySelector('.tint').style.fill = 'red'
            if (this.cord[0].length - 1 === key)
                template.querySelector('.tint').style.fill = '#00ff00'
            const marker = new YMapMarker(
                {
                    coordinates: el,
                    draggable: true,
                    mapFollowsOnDrag: true,
                    onFastClick: this.remove.bind(data),
                    onDragEnd: this.onDragEnd.bind(data),
                },
                template
            )
            this.markers.push(marker)
            this.admin.map.addChild(marker)
        })
    }


    remove(data){
        if(confirm('Вы точно хотите удалить маркер?')){
            const polygon = this.class.polygons.filter(el=>el.id === this.id)[0]
            polygon.cord[0].splice(this.key, 1)
            this.class.set(polygon,polygon.cord)
            axios.post('/admin/map/marker/zone/'+this.id,{
                cord: polygon.cord
            }).then(({data})=>{
                if(data.status){
                    this.class.dataNode.value = JSON.stringify(data.polygons)
                }
            })
        }
    }

    onDragEnd(coordinates){
        const data = this.class.polygons.filter(el=>el.id === this.id)[0]
        this.class.set(data,data.cord,true)
        this.class.cord[0][this.key] = coordinates
        this.class.setPolygon()
        this.class.open(this.id)
    }

    setPolygon(){
        const {YMapFeature} = ymaps3;

        const polygon = new YMapFeature({
            geometry: {
                type: 'Polygon',
                coordinates: this.cord,
            },
            onClick: this.open.bind(this,this.id),
            style: {stroke: [{color: this.color, width: this.width}],cursor: 'pointer', fill: this.fill}
        });
        const dataPolygon = {
            title: this.title,
            color: this.color,
            description: this.description,
            width: this.width,
            fill: this.fill,
            id: this.id,
            cord: this.cord,
            markers: this.markers,
            polygon
        }
        this.polygons = this.polygons.filter(el=>el.id !== this.id)
        this.polygons.push(dataPolygon)
        if(this.polygon)
            this.admin.map.removeChild(this.polygon);
        this.polygon = polygon
        this.admin.map.addChild(polygon);
    }

    save(id = ''){
        const title = this.template.querySelector('input[name=title]').value
        const description = this.template.querySelector('textarea[name=description]').value
        const url = id ? '/admin/map/zone/'+id : '/admin/map/zone';
        axios.post(url,{
            title,
            description,
            cords: this.cord,
            color: this.color,
            fill: this.fill,
            width: this.width,
        }).then(({data})=>{
            this.polygons.forEach((el,key)=> {
                if (el.id === 0) {
                    this.polygons[key].id = data.zone.id
                    this.polygons[key].title = data.zone.title
                    this.polygons[key].description = data.zone.description
                }

            })
            const dataNode = [{
                title: data.zone.title,
                id: data.zone.id,
                description: data.zone.description,
                fill: data.zone.fill,
                width: data.zone.width,
                color: data.zone.color,
                cords: data.zone.cords,
            }]
            JSON.parse(this.dataNode.value).forEach(el=>{
                if(id != el.id)
                    dataNode.push(el)
            })
            this.dataNode.value = JSON.stringify(dataNode)
            this.close()
        })
    }
}

export default Zone
