export class Map {
    constructor() {
        this.initMap();
    }

    setCenter(markersData) {
        let containerWidth = document.querySelector('.container_map').clientWidth;
        let markers = markersData ?? JSON.parse(document.getElementById('markers').value);
        let minLat = Infinity;
        let maxLat = -Infinity;
        let minLng = Infinity;
        let maxLng = -Infinity;

        markers.forEach(function(marker) {
            let cord = JSON.parse(marker.cord);
            let lat = cord[1];
            let lng = cord[0];
            minLat = Math.min(minLat, lat);
            maxLat = Math.max(maxLat, lat);
            minLng = Math.min(minLng, lng);
            maxLng = Math.max(maxLng, lng);
        });

        let centerLat = (minLat + maxLat) / 2;
        let centerLng = (minLng + maxLng) / 2;

        let center = [centerLng, centerLat];

        let bounds = [[minLng, minLat], [maxLng, maxLat]];
        let zoom = this.getBoundsZoomLevel(bounds, { width: containerWidth, height: 400 });

        return { center, zoom };
    }

    getBoundsZoomLevel(bounds, mapDim) {
        const WORLD_DIM = {height: 256, width: 256};
        let ZOOM_MAX = 21;

        function latRad(lat) {
            const sin = Math.sin(lat * Math.PI / 180);
            const radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        let ne = bounds[1]; // Получаем северо-восточную точку
        let sw = bounds[0]; // Получаем юго-западную точку

        let latFraction = (latRad(ne[1]) - latRad(sw[1])) / Math.PI;

        let lngDiff = ne[0] - sw[0];
        let lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        let latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
        let lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }

    async initMap() {
        await ymaps3.ready;
        const { YMap, YMapDefaultSchemeLayer, YMapDefaultFeaturesLayer } = ymaps3;


        let { center, zoom } = this.setCenter(); // Получаем центр и зум
        console.log(center, zoom);
        this.map = new YMap(
            document.getElementById('map'),
            {
                location: {
                    center: center,
                    zoom: zoom
                },
                showScaleInCopyrights: true
            },
            [
                new YMapDefaultSchemeLayer({}),
                new YMapDefaultFeaturesLayer({})
            ]
        );

        this.map.addChild(new YMapDefaultSchemeLayer());
    }
}
