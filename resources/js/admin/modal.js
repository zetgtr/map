import {ModalWindow} from "../../../../public/assets/js/admin/page/Data/modal/newModalWindow";


class Modal{
    constructor(template) {
        this.template = template
        this.modal = new ModalWindow(document.getElementById('modaldemo8'), true)
        this.btn = this.template.querySelector('.tinymce')
        this.textarea = this.template.querySelector('#description')
        this.addEvent()
    }

    addEvent(){
        this.btn.addEventListener('click',this.openModal.bind(this))
    }

    openModal(){
        this.modal.modal_node.querySelector('.modal-dialog').classList.add('modal-xl');
        const texteria = document.createElement('textarea')
        texteria.classList.add('my-editor');
        this.modal.insertTitle('Описание');
        this.modal.insertNodeBody(texteria);
        tinymce.init(editor_config)
        this.modal.openModal()

        this.modal.opened_modal = () => {
            tinymce.activeEditor.setContent(this.textarea.value)
            this.fixModal(texteria)
        }
        this.modal.close_modal = () => {
            this.modal.modal_node.style.display = 'none'
        }
        this.modal.save_modal = this.save.bind(this)

    }

    fixModal(){
        const tinaNode = this.modal.modal_node.querySelector(".tox-tinymce");
        if(tinaNode){
            const srcCodeOpenBtnNode = tinaNode.querySelector(
                'button[aria-label="Исходный код"]'
            );
            const srcLinkBtnNode = tinaNode.querySelector(
                'button[aria-label="Вставить/редактировать ссылку"]'
            );
            srcCodeOpenBtnNode.addEventListener("click", this.openEvent.bind(this));
            srcLinkBtnNode.addEventListener("click", this.openEvent.bind(this));
        }
    }


    openEvent(){
        const modalNode = this.modal.modal_node;
        modalNode.style.display = "none";
        setTimeout(() => {
            const srcCodeControlBtnsNodes =
                document.querySelectorAll(".tox-button");

            srcCodeControlBtnsNodes.forEach((srcCodeControlBtn) => {
                srcCodeControlBtn.addEventListener("click", () => {
                    modalNode.style.display = "block";
                });
            });
        }, 0);
    }

    save(){
        this.textarea.value = tinymce.activeEditor.getContent()
        this.modal.closeModal()
    }
}

export default Modal
