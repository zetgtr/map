import {AddPosition} from "./addPosition";

class Marker {
    constructor(addPosition) {
        this.addPosition = addPosition
    }

    marker = (feature) => {
        const { YMapMarker } = ymaps3;
        const endThis = {
            class: this.addPosition,
            feature
        }
        return new YMapMarker(
            {
                coordinates: feature.geometry.coordinates,
                draggable: true,
                mapFollowsOnDrag: true,
                onClick: this.onClick.bind(this.addPosition,feature),
                // onDragStart: this.onDragStart.bind(this.addPosition,feature),
                onDragEnd: this.onDragEnd.bind(endThis),
            },
            feature.container
        )
    }


    cluster = (coordinates,features) => {
        const { YMapMarker } = ymaps3;
        return new YMapMarker(
            {
                coordinates: coordinates
            },
            this.circle(features.length)
        )
    }

    circle(count) {
        const circle = document.createElement('div');
        circle.classList.add('circle');
        circle.innerHTML = `
                  <div class="circle-content item_map">
                      <span class="circle-text">${count}</span>
                  </div>
              `;
        return circle;
    }

    onClick(feature){
        console.log(feature.id)
        const dataMarkers = JSON.parse(this.markersNode.value);
        const item = dataMarkers.filter(el => el.id === feature.id)[0];
        this.el = feature.container;
        console.log(feature.id);
        this.open(feature.id);
        const addressNode = this.template.querySelector('textarea[name=address]');
        addressNode.dataset.cord = item.cord;
    }

    onDragStart(feature){
        this.el = feature.container;
        this.open(feature.id);
    }

    onDragEnd(cord){
        console.log(this.class)
        this.class.open(this.feature.id);
        const addressNode = this.class.template.querySelector('textarea[name=address]');
        addressNode.dataset.cord = JSON.stringify(cord);
        this.class.cord = cord;
        ymaps.geocode([this.class.cord[1], this.class.cord[0]]).then((res) => {
            let dataMarkers = JSON.parse(this.class.markersNode.value);
            const item = dataMarkers.filter((el) => el.id === this.feature.id)[0];
            const geoObject = res.geoObjects.get(0);

            const addressLine = geoObject.getAddressLine();
            if (item) {
                dataMarkers = dataMarkers.filter((el) => el.id !== this.feature.id);
                item.address = addressLine;
                item.cord = JSON.stringify(this.class.cord);
                dataMarkers.push(item);
                this.class.markersNode.value = JSON.stringify(dataMarkers);
            }
            this.class.markers.forEach((el,key)=>{
                if(el.id === this.feature.id){
                    this.class.markers[key].geometry.coordinates = cord
                    this.class.createClusterer()
                }
            })
            const addressNode = this.class.template.querySelector('textarea[name=address]');
            this.address = addressLine;

            addressNode.value = addressLine;
        });


    }
}

export default Marker
