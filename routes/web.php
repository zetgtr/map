<?php


use Illuminate\Support\Facades\Route;
use Map\Http\Controllers\FrontController;
use Map\Http\Controllers\MapController;
use Map\Http\Controllers\MapCategoryController;
use Map\Http\Controllers\MapTypeController;
use Map\Http\Controllers\MapIconController;
use Map\Http\Controllers\MapSettingsController;
use Map\Http\Controllers\MapZoneController;
use Map\Models\MapSettings;


Route::middleware('web')->group(function (){
   Route::group(['prefix'=>"admin", 'as'=>'admin.', 'middleware' => ['menu.check:1001','is_admin']],static function() {
       Route::get('map', [MapController::class, 'index'])->name('map')->middleware('map_settings');
       Route::group(['prefix' => "map", 'as' => 'map.',], static function () {
           Route::post('zone',[MapZoneController::class,'store']);
           Route::post('zone/{map_zone}',[MapZoneController::class,'update']);
           Route::delete('zone/{map_zone}',[MapZoneController::class,'delete']);
           Route::post('marker/zone/{map_zone}',[MapZoneController::class,'delete_marker']);
           Route::post('/', [MapController::class, 'store'])->name('store')->middleware('map_settings');
           Route::post('/marker/{map_marker}', [MapController::class, 'update'])->name('update')->middleware('map_settings');
           Route::delete('/marker/{map_marker}', [MapController::class, 'destroy'])->name('destroy')->middleware('map_settings');
           Route::resource('category', MapCategoryController::class)->middleware('map_settings');
           Route::resource('type', MapTypeController::class)->middleware('map_settings');
           Route::resource('icon', MapIconController::class)->middleware('map_settings');
           Route::post('order/category',[MapCategoryController::class,'order'])->name('category.order');
           Route::get('publish/category/{map_category}',[MapCategoryController::class,'publish'])->name('category.publish');
           Route::resource('settings', MapSettingsController::class);
       });
   });
    try {
        $settings = MapSettings::first();
        Route::get($settings->seo_url,[FrontController::class,'index'])->name('map.front');
    }catch (Exception $exception){
        
    }
   
});

