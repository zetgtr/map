<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class CategoryCreateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('map.request.category');
    }

    public function authorize(): bool
    {
        return true;
    }
}
