<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MarkerUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('map.request.marker');
    }

    protected function prepareForValidation(){
        if (!$this->type_id){
            $this->merge([
                'type_id' => null
            ]);
        }
        if (!$this->category_id){
            $this->merge([
                'category_id' => null
            ]);
        }
    }

    public function authorize(): bool
    {
        return true;
    }
}
