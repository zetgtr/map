<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MapSettingsRequest extends FormRequest
{
    public function rules(): array
    {
        return config('map.request.settings');
    }

    protected function prepareForValidation()
    {
        $this->merge([
            'btn_category_check' => (boolean) $this->btn_category_check,
            'btn_type_check' => (boolean) $this->btn_type_check,
            'btn_point_check' => (boolean) $this->btn_point_check,
            'btn_search_check' => (boolean) $this->btn_search_check
        ]);
    }

    public function authorize(): bool
    {
        return true;
    }
}
