<?php

namespace Map\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class TypeUpdateRequest extends FormRequest
{
    public function rules(): array
    {
        return config('map.request.type');
    }

    public function authorize(): bool
    {
        return true;
    }
}
