<?php

namespace Map\Providers;

use Catalog\Http\Middleware\SetCookieMiddleware;
use Catalog\QueryBuilder\CatalogBuilder;
use Catalog\QueryBuilder\QueryBuilder;
use Catalog\View\filter\FrontFilter;
use Catalog\View\Order;
use Catalog\View\Settings;
use Catalog\View\edit\Content as EditContent;
use Catalog\View\product\create\Content as ProductCreateContent;
use Catalog\View\product\edit\Content as ProductEditContent;
use Catalog\View\product\Index;
use Catalog\View\create\Content as CreateContent;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\AliasLoader;
use Catalog\Http\Middleware\Auth as PacageAuth;
use Catalog\QueryBuilder\CartBuilder;
use Catalog\View\filter\Category;
use Catalog\View\filter\Filter;
use Catalog\View\front\Cart;
use Catalog\View\front\Cartitem;
use Catalog\View\front\CategoryFilter;
use Catalog\View\front\PopularFilter;
use Catalog\View\front\PriceFilter;
use Illuminate\Http\Request;
use Illuminate\View\Middleware\ShareErrorsFromSession;
use Map\Http\Middleware\SettingsMiddleware;

class MapServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any package services.
     *
     * @return void
     */
    public function boot()
    {
        if ($this->app->runningInConsole()) {
            $this->loadMigrationsFrom(__DIR__ . '/../../database/migrations');

        }

        $this->app['router']->aliasMiddleware('key_cart', SetCookieMiddleware::class);
        $this->app['router']->aliasMiddleware('auth_pacage', PacageAuth::class);
        $this->app['router']->aliasMiddleware('map_settings', SettingsMiddleware::class);

        $this->publishes([
            __DIR__ . '/../../database/migrations' => database_path('migrations'),
            __DIR__ . '/../../resources/scss' => resource_path('sass/components'),
        ], 'migrations');

        $this->publishes([
            __DIR__.'/../../config/map.php' => config_path('map.php'),
        ], 'map_config');

        $this->publishes([
            __DIR__.'/../../resources/views' => resource_path('views/vendor/map'),
        ], 'map_views');

        $this->publishes([
            __DIR__.'/../../resources/js' => resource_path('js/map'),
        ], 'map_script');

        $this->publishes([
            __DIR__.'/../../resources/icons' => storage_path('app/public/map/icons'),
        ], 'map_icons');

        $this->publishes([
            __DIR__.'/../../resources/scss' => resource_path('assets/scss/map'),
        ], 'map_scss');

        $this->loadViewsFrom(__DIR__.'/../../resources/views', 'map');
        $this->loadViewsFrom(__DIR__.'/../../resources/components', 'map');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/web.php');
        $this->loadRoutesFrom(__DIR__ . '/../../routes/api.php');
        $this->components();
    }

    private function components()
    {
        Blade::component(\Map\View\Admin\Settings::class, 'map::map.settings');
        Blade::component(\Map\View\Admin\Icons::class, 'map::type.icons');

        Blade::component(\Map\View\Admin\Type::class, 'map::type.type');
    }

    private function singletons()
    {

    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(QueryBuilder::class, CatalogBuilder::class);
        $this->mergeConfigFrom(__DIR__.'/../../config/map.php', 'map');
        $this->singletons();
    }
}
