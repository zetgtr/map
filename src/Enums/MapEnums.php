<?php

namespace Map\Enums;

enum MapEnums: string
{
    case CATEGORY = "categories";
    case CONSTRUCTOR = "constructor";
    case TYPE = "type";
    case SETTINGS = "settings";


    case SEO = 'seo';
    case BUTTONS = 'buttons';
    case KEY = 'key';
    
    case ICONS = 'icons';

    public static function all(): array
    {
        return [
            self::CATEGORY->value,
            self::CONSTRUCTOR->value,
            self::SETTINGS->value
        ];
    }
}
