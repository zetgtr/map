<?php

namespace Map\Models;

use Illuminate\Database\Eloquent\Model;

class MapIcon extends Model
{
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        $this->fillable = config('map.fillable.icon');
        parent::__construct($attributes);
    }
}
