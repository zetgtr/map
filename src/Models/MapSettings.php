<?php

namespace Map\Models;

use Illuminate\Database\Eloquent\Model;

class MapSettings extends Model
{
    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->fillable = config('map.fillable.settings');
    }
}
