<?php

namespace Map\Models;

use Illuminate\Database\Eloquent\Model;

class MapType extends Model
{
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        $this->fillable = config('map.fillable.type');
        parent::__construct($attributes);
    }

    public function icons(){
        return $this->belongsTo(MapIcon::class,'icon','id');
    }
}
