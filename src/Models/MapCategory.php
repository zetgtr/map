<?php

namespace Map\Models;

use Illuminate\Database\Eloquent\Model;

class MapCategory extends Model
{
    protected $fillable = [];
    public function __construct(array $attributes = [])
    {
        $this->fillable = config('map.fillable.category');
        parent::__construct($attributes);
    }

    public function markers(){
        return $this->hasMany(MapMarker::class,'category_id','id')->with('type.icons');
    }
}
