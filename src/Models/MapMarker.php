<?php

namespace Map\Models;

use Illuminate\Database\Eloquent\Model;

class MapMarker extends Model
{
    protected $fillable = [];

    public function __construct(array $attributes = [])
    {
        $this->fillable = config('map.fillable.marker');
        parent::__construct($attributes);
    }


    public function type(){
        return $this->belongsTo(MapType::class,'type_id','id');
    }
}
