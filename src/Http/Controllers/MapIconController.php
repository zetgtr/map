<?php

namespace Map\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\File;
use Map\Models\MapIcon;
use Map\Requests\Admin\IconCreateRequest;

class MapIconController extends Controller
{

    public function store(IconCreateRequest $request)
    {
        MapIcon::create($request->validated());

        return redirect()->back()->with('success','Иконка успешно добавлена');
    }

    public function destroy(MapIcon $icon){
        $imageUrl = public_path($icon->url);
        if (File::exists($imageUrl)) {
            File::delete($imageUrl);
        }

        $icon->delete();

        return response()->json(['status'=>true,'message' => 'Изображение успешно удалено'], 200);
    }
}
