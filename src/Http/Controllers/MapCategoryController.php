<?php

namespace Map\Http\Controllers;

use Catalog\Enums\CatalogEnums;
use App\Http\Controllers\Controller;
use Catalog\Requests\Category\CreateRequest;
use Catalog\Requests\Category\UpdateRequest;
use Catalog\Models\Category;
use Catalog\QueryBuilder\CatalogBuilder;
use Illuminate\Http\Request;
use Map\Enums\MapEnums;
use Map\Models\MapCategory;
use Map\QueryBuilder\MapBuilder;
use Map\Requests\Admin\CategoryCreateRequest;

class MapCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MapBuilder $builder)
    {
        return view('map::map.category.index',[
            'links' => $builder->getNavigationLinks(MapEnums::CATEGORY->value),
            'categories' => MapCategory::orderBy('order')->get()
        ]);
    }

    public function order(Request $request){
        foreach($request->all()['items'] as $key=>$item){
            MapCategory::where('id',$item['id'])->update(['order'=>$key]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.category.create',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::CATEGORY->value),
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value)
        ]);
    }
    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryCreateRequest $request)
    {
        $category = MapCategory::create($request->validated());
        if ($category) {
            return \redirect()->back()->with('success', __('messages.admin.catalog.category.store.success'));
        }

        return \redirect()->back()->with('error', __('messages.admin.catalog.category.store.fail'));
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    public function getCatalog(string $params,CatalogBuilder $catalogBuilder){
        return $catalogBuilder->getCatalog($params);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Category $category,CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.category.edit',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::CATEGORY->value),
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value),
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Category $category)
    {

        $category = $category->fill($request->validated());
        if ($category->save()) {
            return \redirect()->route('admin.catalog.category.edit',['category'=>$category])->with('success', __('messages.admin.catalog.category.update.success'));
        }

        return \back()->with('error', __('messages.admin.catalog.category.update.fail'));
    }

    public function publish(int $id)
    {
        $category = MapCategory::find($id);
        $category->publish = !$category->publish;
        if ($category->save()) return ['status' => true, 'publish' => $category->publish];
        else  return ['status' => false];
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Category $category)
    {
        try {
            $category->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.category.destroy.success')];
        } catch (\Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.category.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }
}
