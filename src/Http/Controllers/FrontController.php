<?php

namespace Map\Http\Controllers;

use App\Http\Controllers\Controller;
use Map\Models\MapCategory;
use Map\Models\MapMarker;
use Map\Models\MapSettings;
use Map\Models\MapType;
use Map\Models\MapZone;

class FrontController extends Controller
{
    public function index()
    {
        return view('map::front.index',[
            'settings'=>MapSettings::first(),
            'markers' => MapMarker::with('type.icons')->get(),
            'zones' => MapZone::get(),
            'categories' => MapCategory::orderBy('order')->get(),
            'types' => MapType::get(),
            ]);
    }
}
