<?php

namespace Map\Http\Controllers;

use App\Models\Admin\Catalog\Filter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Map\Enums\MapEnums;
use Map\Models\MapCategory;
use Map\Models\MapMarker;
use Map\Models\MapSettings;
use Map\Models\MapType;
use Map\Models\MapZone;
use Map\QueryBuilder\MapBuilder;
use Map\Requests\Admin\MarkerCreateRequest;
use Map\Requests\Admin\MarkerUpdateRequest;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MapBuilder $builder)
    {
        return view('map::map.constructor.index',[
            'links' => $builder->getNavigationLinks(MapEnums::CONSTRUCTOR->value),
            'markers' => MapMarker::with(['type.icons'])->get(),
            'zone_data' => MapZone::get(),
            'categories' => MapCategory::orderBy('order')->get(),
            'types' => MapType::get(),
            'settings' => MapSettings::first()
        ]);
    }

    public function search(string $text, CatalogBuilder $catalogBuilder)
    {
        return $catalogBuilder->search($text);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.product.create',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MarkerCreateRequest $request)
    {
        $marker = MapMarker::create($request->validated());
        if($marker) {
            if ($marker->type)
                $marker->type->icons;
            return ['status' => true, 'data' => $marker];
        }

        return ['status'=>false];
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id,CatalogBuilder $catalogBuilder)
    {
        $data = $catalogBuilder->getProductCategory($id);

        return view('catalog::catalog.product.show',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'categories'=>$data['category'],
            'category' => $id,
            'products' => $data['products'],
            'breadcrumb' => $catalogBuilder->getProductBreadcrumb($id)
            ]);
    }

    public function filter(FilterBuilder $builder,FilterRequest $request)
    {

        $builder->setFilter($request);
        return $builder->getCount();
    }

    public function filterView(FilterBuilder $builder,FilterRequest $request)
    {
        $builder->setFilter($request);
        return $builder->getFilterView($request);
    }

    public function order(Request $request, CatalogBuilder $catalogBuilder){
        $catalogBuilder->setOrderProduct($request->all()['items'],$request->all()['category']);
    }

    public function publish(Product $product,Request $request)
    {
        $category = Category::find($request->get('category'));
        $category->products()->updateExistingPivot($product->id, ['publish' => ! $category->products()->where('id', $product->id)->first()->pivot->publish]);
        if ($category->save()) return ['status' => true, 'publish' =>  $category->products()->where('id', $product->id)->first()->pivot->publish];
        else  return ['status' => false];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product,CatalogBuilder $catalogBuilder)
    {
        return view('catalog::catalog.product.edit',[
            'links' => $catalogBuilder->getNavigationLinks(CatalogEnums::PRODUCT->value),
            'product'=>$product,
            'navigation' => $catalogBuilder->getNavigationPageLink(CatalogEnums::CONTENT->value)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(MarkerUpdateRequest $request, MapMarker $mapMarker)
    {
        if($mapMarker->update($request->validated())){
            if ($mapMarker->type)
                $mapMarker->type->icons;
            return ['status' => true, 'data' => $mapMarker];
        }
        return ['status'=>false];
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(MapMarker $mapMarker)
    {
        try {
            $mapMarker->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.product.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.product.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }
}
