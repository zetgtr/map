<?php

namespace Map\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Map\Models\MapZone;
use Map\Requests\Admin\ZoneCreateRequest;
use Map\Requests\Admin\ZoneUpdateRequest;

class MapZoneController extends Controller
{
    public function store(ZoneCreateRequest $request)
    {
        $zone = MapZone::create($request->validated());
        return ['status'=>true,'zone'=>$zone];
    }
    public function update(ZoneUpdateRequest $request,int  $id)
    {
        $zone = MapZone::findOrFail($id);
        $zone->update($request->validated());
        return ['status'=>true,'zone'=>$zone];
    }
    public function delete(int $id)
    {
        $zone = MapZone::findOrFail($id);
        $zone->delete();
        return ['status'=>true,'polygons'=>MapZone::get()];
    }
    public function delete_marker(int $id, Request $request)
    {
        $zone = MapZone::findOrFail($id);
        if(count($request->cord[0]))
            $zone->update(['cords'=>$request->cord]);
        else
            $zone->delete();
        return ['status'=>true,'polygons'=>MapZone::get()];
    }
}
