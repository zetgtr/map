<?php

namespace Map\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Map\Enums\MapEnums;
use Map\Models\MapSettings;
use Map\QueryBuilder\MapBuilder;
use Map\Requests\Admin\MapSettingsRequest;

class MapSettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MapBuilder $builder)
    {
        return view("map::map.settings.index",[
            'links'=>$builder->getNavigationLinks(MapEnums::SETTINGS->value),
            'settingsLinks'=>$builder->getNavigationLinksSettings(MapEnums::KEY->value),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(MapSettingsRequest $request)
    {
        if(MapSettings::updateOrCreate(['id' => 1],$request->validated()))
            return redirect()->back()->with('success','Успешно сохранено');

        return redirect()->back()->with('error','Ошибка сохранения');
    }
}
