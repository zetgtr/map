<?php

namespace Map\Http\Controllers;

use App\Models\Admin\Catalog\Filter;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Map\Enums\MapEnums;
use Map\Models\MapIcon;
use Map\Models\MapMarker;
use Map\Models\MapSettings;
use Map\Models\MapType;
use Map\QueryBuilder\MapBuilder;
use Map\Requests\Admin\MarkerCreateRequest;
use Map\Requests\Admin\MarkerUpdateRequest;
use Map\Requests\Admin\TypeCreateRequest;
use Map\Requests\Admin\TypeUpdateRequest;

class MapTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MapBuilder $builder)
    {
        return view('map::map.type.index',[
            'links' => $builder->getNavigationLinks(MapEnums::TYPE->value),
            'typeLinks'=>$builder->getNavigationLinksType(MapEnums::TYPE->value)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TypeCreateRequest $request)
    {
        MapType::create($request->validated());
        return redirect()->back()->with('success','Успешно добавлено');
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(int $id)
    {
        return ['status'=>true,'type'=>MapType::find($id)];
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(TypeUpdateRequest $request, int $id)
    {
        $type = MapType::find($id);
        if($type->update($request->validated())){
            return redirect()->back()->with('success','Успешно обновлено');
        }
        return redirect()->back()->with('error','Ошибка обновления');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $type = MapType::find($id);
        try {
            $type->delete();
            $response = ['status' => true,'message' => __('messages.admin.catalog.product.destroy.success')];
        } catch (Exception $exception)
        {
            $response = ['status' => false,'message' => __('messages.admin.catalog.product.destroy.fail').$exception->getMessage()];
        }

        return $response;
    }
}
